package spellingbee.client;

import javafx.scene.control.Label;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import spellingbee.network.Client;

/**
 * This class is a GameTab object that extends the JavaFX Tab object.
 * 
 * @author Estefan Maheux-Saban 1931517
 */
public class ScoreTab extends Tab {
	private Client clnt;
	private String[] brackets;
	private String score;
	private Label scoreField;
	private Label queenBee, genius, amazing, good, started;

	/**
	 * Constructor that initializes the ScoreTab object in function of the client.
	 * sets the parameters by using helper methods.
	 * 
	 * @param clnt
	 */
	public ScoreTab(Client clnt) {
		super("Score");
		this.clnt = clnt;
		this.score = clnt.sendAndWaitMessage("getScore");
		this.brackets = clnt.sendAndWaitMessage("getBrackets").split(" ");
		GridPane gridpane = new GridPane();
		gridpane.add(rating(), 0, 0);
		gridpane.add(scores(), 1, 0);
		gridpane.setHgap(10);

		setContent(gridpane);

	}

	/**
	 * refresh method updates the GUI to display the current score of the user.
	 * 
	 */
	public void refresh() {
		score = clnt.sendAndWaitMessage("getScore");
		this.scoreField.setText(score);
		if (Integer.parseInt(brackets[0]) <= Integer.parseInt(score)) {
			started.setTextFill(Color.BLACK);
		}
		if (Integer.parseInt(brackets[1]) <= Integer.parseInt(score)) {
			good.setTextFill(Color.BLACK);
		}
		if (Integer.parseInt(brackets[2]) <= Integer.parseInt(score)) {
			amazing.setTextFill(Color.BLACK);
		}
		if (Integer.parseInt(brackets[3]) <= Integer.parseInt(score)) {
			genius.setTextFill(Color.BLACK);
		}
		if (Integer.parseInt(brackets[4]) <= Integer.parseInt(score)) {
			queenBee.setTextFill(Color.BLACK);
		}

	}

	/**
	 * helper method that creates labels to title all the brackets and current score
	 * of the user inside of the ScoreTab
	 * 
	 * @return VBox that contains all the labels
	 */
	public VBox rating() {
		queenBee = new Label("QueenBee");
		genius = new Label("Genius");
		amazing = new Label("Amazing");
		good = new Label("Good");
		started = new Label("Getting Started");
		Label curr = new Label("Current Score: ");
		queenBee.setTextFill(Color.GREY);
		genius.setTextFill(Color.GREY);
		amazing.setTextFill(Color.GREY);
		good.setTextFill(Color.GREY);
		started.setTextFill(Color.GREY);

		VBox rates = new VBox(queenBee, genius, amazing, good, started, curr);
		return rates;
	}

	/**
	 * helper method that display the percentages of the score and
	 *  the current score of the user.
	 * 
	 * @return VBox that contains all the labels
	 */
	public VBox scores() {
		Label queenBeescr, geniusscr, amazingscr, goodscr, startedscr, currScore;
		queenBeescr = new Label(brackets[4]);
		geniusscr = new Label(brackets[3]);
		amazingscr = new Label(brackets[2]);
		goodscr = new Label(brackets[1]);
		startedscr = new Label(brackets[0]);
		currScore = new Label(score);
		currScore.setTextFill(Color.RED);
		scoreField = currScore;

		VBox rates = new VBox(queenBeescr, geniusscr, amazingscr, goodscr, startedscr, currScore);
		return rates;
	}

}
