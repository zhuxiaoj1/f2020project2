package spellingbee.client;

import java.util.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import spellingbee.network.*;

/**
 * This class is a GameTab object that extends the JavaFX Tab object.
 * 
 * @author Danilo Zhu 1943382
 */
public class GameTab extends Tab {
	private Client clnt;
	private Button lt1, lt2, lt3, lt4, lt5, lt6, lt7;
	private ArrayList<Button> btnArr = new ArrayList<Button>();
	TextField input = new TextField("Type a word!");
	TextField scoreField = new TextField("0");

	/**
	 * This method creates a Game Tab in the application and handles the Spelling
	 * Bee game. It sets all the buttons and fields with the use of helper methods.
	 * It also has an Event Handler for the submit button.
	 * 
	 * @param clnt Client that relays information to the server.
	 */
	public GameTab(Client clnt) {
		super("Game", new Label("Play the game"));
		this.clnt = clnt;

		HBox btnContainer = setBtns();
		HBox txtContainer = new HBox(input);

		Button submitBtn = new Button("Submit!");

		TextField outcome = new TextField("Welcome to Spelling Bee!");

		HBox outScore = new HBox(outcome, scoreField);

		submitBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				outcome.setText(clnt.sendAndWaitMessage(input.getText()));
				scoreField.setText("Score: " + clnt.sendAndWaitMessage("getScore"));
				input.setText("");
			}
		});

		VBox container = new VBox(btnContainer, txtContainer, submitBtn, outScore);
		colorCenter();

		setContent(container);
	}

	/**
	 * This method sets all the letters into separate buttons and sets the buttons
	 * in an HBox that it then returns.
	 * 
	 * @return An HBox with all the buttons set in it.
	 */
	private HBox setBtns() {
		String ltSet = clnt.sendAndWaitMessage("getLetters");
		lt1 = new Button("" + ltSet.charAt(0));
		lt2 = new Button("" + ltSet.charAt(1));
		lt3 = new Button("" + ltSet.charAt(2));
		lt4 = new Button("" + ltSet.charAt(3));
		lt5 = new Button("" + ltSet.charAt(4));
		lt6 = new Button("" + ltSet.charAt(5));
		lt7 = new Button("" + ltSet.charAt(6));
		Button deleteBtn = new Button("Delete");
		Button clearBtn = new Button("Clear");

		// Setting the buttons in a ArrayList to iterate through them easily.
		btnArr.add(lt1);
		btnArr.add(lt2);
		btnArr.add(lt3);
		btnArr.add(lt4);
		btnArr.add(lt5);
		btnArr.add(lt6);
		btnArr.add(lt7);
		btnArr.add(deleteBtn);
		btnArr.add(clearBtn);

		for (Button i : btnArr) {
			pressLetterEvt(i, i.getText());
		}

		HBox btnContainer = new HBox(lt1, lt2, lt3, lt4, lt5, lt6, lt7, deleteBtn, clearBtn);

		return btnContainer;
	}

	/**
	 * This method colors the center letter in red.
	 */
	private void colorCenter() {
		String center = clnt.sendAndWaitMessage("getCenter");

		for (Button i : btnArr) {
			if (i.getText().equals(center)) {
				i.setTextFill(Color.web("#ff0000"));
			}
		}
	}

	/**
	 * This method assigns event handlers to each button in the button ArrayList,
	 * that way when they are displayed, they can already be pressed.
	 * 
	 * @param btn The button that needs to have an action assigned to it.
	 * @param ltr The content of the button. (Generally a letter.)
	 * @return A Button with an action assigned to it.
	 */
	private Button pressLetterEvt(Button btn, String ltr) {
		if (ltr.equals("Clear")) {
			btn.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent evt) {
					input.setText("");
				}
			});
		} else if (ltr.equals("Delete")) {
			btn.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent evt) {
					String inputText = input.getText();
					inputText = inputText.substring(0, inputText.length() - 1);
					input.setText(inputText);
				}
			});
		} else {
			btn.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent evt) {
					String inputText = input.getText();
					inputText = inputText + ltr;
					input.setText(inputText);
				}
			});
		}

		return btn;
	}
	
	public TextField getScoreField() {
		return scoreField;
	}
}
