package spellingbee.client;

/**
 * This interface is used so that client code and server code can be tested at
 * the same time.
 * 
 * @author Danilo Zhu and Estefan Maheux-Saban
 */
public interface ISpellingBeeGame {
	int getPointsForWord(String attempt);

	String getMessage(String attempt);

	String getAllLetters();

	char getCenterLetter();

	int getScore();

	int[] getBrackets();

	int[] setOfScores();
}
