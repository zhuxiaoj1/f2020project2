package spellingbee.client;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import spellingbee.network.Client;

/**
 * This class is the Graphical User Interface of the Spelling Bee game.
 * 
 * @author Danilo Zhu and Estefan Maheux-Saban
 */
public class SpellingBeeClient extends Application {
	private Client clnt = new Client();
	GameTab gameTab = new GameTab(clnt);
	ScoreTab scoreTab = new ScoreTab(clnt);

	/**
	 * The main method where the application is launched.
	 * 
	 * @param args Command-line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * This method is where all the tabs are defined and are set to be displayed.
	 * 
	 * @param root The base stage of the application.
	 */
	@Override
	public void start(Stage root) throws Exception {
		TabPane sbApp = new TabPane();

		sbApp.setMinHeight(250);
		sbApp.setMinWidth(350);

		sbApp.getTabs().add(gameTab);
		sbApp.getTabs().add(scoreTab);
		
		gameTab.getScoreField().textProperty().addListener(new ChangeListener<String>() {
		    public void changed(ObservableValue<? extends String> observable,
		            String oldValue, String newValue) {

		        scoreTab.refresh();
		    }
		});

		HBox container = new HBox(sbApp);
		Scene scene = new Scene(container);

		root.setScene(scene);
		root.setTitle("Spelling Bee Game");

		root.show();
		
	}
}