package spellingbee.client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import javafx.scene.control.TextField;

/**
 * This class is a SpellingBeeGame object that implements the ISpellingBeeGame
 * object.
 * 
 * @author Estefan Maheux-Saban 1931517
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	private String letters;
	private int score;
	private HashSet<String> progression = new HashSet<String>();
	private static HashSet<String> possibleWords = createFromFile("..\\datafiles\\english.txt");
	private static Random rng = new Random();
	private int[] setOfScore;

	/**
	 * This method creates a HashSet of all the words provided by the file
	 * 
	 * @param String that represents the location of the file(path)
	 * @return HashSet<String> that includes all the words from the file
	 */

	public static HashSet<String> createFromFile(String path) {
		Path p = Paths.get(path);
		try {
			return new HashSet<String>(Files.readAllLines(p));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * generates 7 letters for the player using the combinations inside of the file
	 * and changes the obligated letter to use in the sets of letters
	 * 
	 * @return String that contains the 7 letters
	 */
	public static String generateLetters() {
		Path p = Paths.get("..\\datafiles\\letterCombinations.txt");
		List<String> rgLetters;
		try {
			rgLetters = Files.readAllLines(p);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		String choice = rgLetters.get(rng.nextInt(rgLetters.size()));
		String[] swap = new String[choice.length()];
		for (int i = 0; i < swap.length; i++) {
			swap[i] = String.valueOf(choice.charAt(i));
		}
		final int middle = 3;
		String oldCenter = swap[middle];
		int randIndex = rng.nextInt(6);
		String newCenter = swap[randIndex];
		swap[middle] = newCenter;
		swap[randIndex] = oldCenter;
		return String.join("", swap);
	}

	/**
	 * Constructors that initializes the the SpellingBeeGame object and sets the
	 * letters, Brackets and progression
	 * 
	 * One constructor uses as an input letters chosen
	 * 
	 */
	public SpellingBeeGame() {
		this.letters = generateLetters();
		this.setOfScore = getBrackets();
		this.progression = new HashSet<String>();

	}

	public SpellingBeeGame(String letters) {
		this.letters = letters;
		this.setOfScore = getBrackets();
		this.progression = new HashSet<String>();
	}

	/**
	 * This method calculates the number of points for an attempt, following the
	 * rules of the Spelling Bee game described in the Project Word Document.
	 * 
	 * @param attempt A String that represents the attempt made by the player.
	 * @return An int that is the number of points that the attempt is worth.
	 */

	@Override
	public int getPointsForWord(String attempt) {
		// TODO Auto-generated method stub
		final int panagram = 7;
		if (attempt.length() == 4) {
			return 1;
		} else if (attempt.length() > 4 && attempt.contains(this.letters)) {
			return attempt.length() + panagram;
		} else {
			return attempt.length();

		}
	}

	/**
	 * This tests the attempt with each of the Spelling Bee game rules and if the
	 * attempt passes all of the tests, returns a success message along with the
	 * score gained. If it does not pass all of the tests, it returns the reason why
	 * it didn't pass.
	 * 
	 * @param attempt A String that represents the attempt made by the player.
	 * @return A String that is the message.
	 */

	@Override
	public String getMessage(String attempt) {
		String regex = "[" + this.letters + "]";
		boolean validation;
		if (attempt.contains(String.valueOf(getCenterLetter()))) {
			if (attempt.length() < 4) {
				return "Your word is too short!";
			} else if (attempt.indexOf(' ') > -1) {
				return "No spaces allowed!";
			}
			for (int i = 0; i < attempt.length(); i++) {
				validation = String.valueOf(attempt.charAt(i)).matches(regex);
				if (!validation) {
					return "You must not use other letters than the ones given!";
				}
			}
			if (progression.contains(attempt)) {
				return "Word already found";
			}
			if (!possibleWords.contains(attempt)) {
				return "Word does not exist!";
			}
			if (getPointsForWord(attempt) > 5) {
				progression.add(attempt);
				score += getPointsForWord(attempt);
				return "Great! " + getPointsForWord(attempt) + " points!";
			} else {
				progression.add(attempt);
				score += getPointsForWord(attempt);
				return "Good! " + getPointsForWord(attempt) + " points!";
			}
		}
		return "You must use the center letter (in red)!";
	}

	/**
	 * all the getters for all the attributes that the objects contain
	 */
	@Override
	public String getAllLetters() {
		// TODO Auto-generated method stub
		return this.letters;
	}

	@Override
	public char getCenterLetter() {
		final int middle = 3;
		return this.letters.charAt(middle);
	}

	@Override
	public int getScore() {
		// TODO Auto-generated method stub
		return this.score;
	}

	@Override
	public int[] getBrackets() {
		// TODO Auto-generated method stub
		for (String i : possibleWords) {
			getMessage(i);
		}
		int[] brackets = { (int) (score * 0.25), (int) (score * 0.50), (int) (score * 0.75), (int) (score * 0.90),
				score };
		this.score = 0;
		return brackets;
	}

	public int[] setOfScores() {
		return this.setOfScore;
	}

}
