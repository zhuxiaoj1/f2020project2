package spellingbee.client;

import java.util.*;

/**
 * This class is a Simple Spelling Bee Game made for testing.
 * 
 * @author Danilo Zhu 1943382
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	private char lt1, lt2, lt3, lt4, lt5, lt6, lt7;
	private int score;
	private HashSet<Character> ltSet;

	/**
	 * This is a constructor for a SimpleSpellingBeeGame object. It initializes each
	 * of the letters to something hard-coded and creates a HashSet containing all
	 * of the letters.
	 */
	public SimpleSpellingBeeGame() { // char lt1, char lt2, char lt3, char lt4, char lt5, char lt6, char lt7
		this.lt1 = 'i';
		this.lt2 = 'a';
		this.lt3 = 'm';
		this.lt4 = 'y';
		this.lt5 = 'c';
		this.lt6 = 's';
		this.lt7 = 'n';

		ltSet = new HashSet<Character>();
		ltSet.add(lt1);
		ltSet.add(lt2);
		ltSet.add(lt3);
		ltSet.add(lt4);
		ltSet.add(lt5);
		ltSet.add(lt6);
		ltSet.add(lt7);
	}

	/**
	 * This method calculates the number of points for an attempt, following the
	 * rules of the Spelling Bee game described in the Project Word Document.
	 * 
	 * @param attempt A String that represents the attempt made by the player.
	 * @return An int that is the number of points that the attempt is worth.
	 */
	@Override
	public int getPointsForWord(String attempt) {
		int length = attempt.length();
		boolean containsAll = false;

		for (char i : ltSet) {
			if (attempt.indexOf(i) == -1) {
				containsAll = false;
				break;
			} else {
				containsAll = true;
			}
		}

		if (length == 4) {
			return 1;
		} else if (containsAll) {
			return length + 7;
		} else {
			return length;
		}
	}

	/**
	 * This tests the attempt with each of the Spelling Bee game rules and if the
	 * attempt passes all of the tests, returns a success message along with the
	 * score gained. If it does not pass all of the tests, it returns the reason why
	 * it didn't pass.
	 * 
	 * @param attempt A String that represents the attempt made by the player.
	 * @return A String that is the message.
	 */
	@Override
	public String getMessage(String attempt) {
		boolean usedOther = false;

		for (int i = 0; i < attempt.length(); i++) {
			if (ltSet.contains(attempt.charAt(i))) {
				usedOther = false;
			} else {
				usedOther = true;
				break;
			}
		}

		if (attempt.indexOf(getCenterLetter()) == -1) {
			return "You must use the center letter (in red)!";
		} else if (attempt.indexOf(' ') > - 1) {
			return "No spaces allowed!";
		} else if (attempt.length() < 4) {
			return "Your word is too short!";
		} else if (usedOther == true) {
			return "You must not use other letters than the ones given!";
		} else {
			if (getPointsForWord(attempt) > 5) {
				score = score + getPointsForWord(attempt);
				return "Great! " + getPointsForWord(attempt) + " points!";
			} else {
				score = score + getPointsForWord(attempt);
				return "Good! " + getPointsForWord(attempt) + " points!";
			}
		}
	}

	/**
	 * This method returns a String with all the letters set in the
	 * SimpleSpellingBeeGame object.
	 * 
	 * @return A String with all the letters.
	 */
	@Override
	public String getAllLetters() {
		return lt1 + " " + lt2 + " " + lt3 + " " + lt4 + " " + lt5 + " " + lt6 + " " + lt7;
	}

	/**
	 * This method returns the letter designated as "center" letter, which must be
	 * used in each of the attempts the player does.
	 * 
	 * @return The center letter.
	 */
	@Override
	public char getCenterLetter() {
		return lt2;
	}

	/**
	 * This method returns the total score of the player.
	 * 
	 * @return An int which is the total score.
	 */
	@Override
	public int getScore() {
		return score;
	}

	/**
	 * This method returns an array of int, which represents the threshold of points
	 * a player can pass.
	 * 
	 * @return An int[] with all the thresholds inside of it.
	 */
	@Override
	public int[] getBrackets() {
		int[] ranking = { 4, 10, 16, 22, 28 };

		return ranking;
	}

}
